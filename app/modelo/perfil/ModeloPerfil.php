<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPerfil extends conexion
{


    public function mostrarDatosPerfilModel($id)
    {
        $tabla = 'usuarios';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM usuarios WHERE id IN(:id)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id, PDO::PARAM_INT);
            if ($preparado->execute()) {
                if ($preparado->rowCount() == 1) {
                    return $preparado->fetch();
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
