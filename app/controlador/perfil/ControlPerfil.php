<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPerfil
{

	private static $instancia;

	public static function singleton_perfil()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}

	public function mostrarDatosPerfilControl($id)
	{
		$datos = ModeloPerfil::mostrarDatosPerfilModel($id);
		return $datos;
	}

	public function guardarPerfilControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['nombre']) &&
			!empty($_POST['nombre'])
		) {
		}
	}
}
