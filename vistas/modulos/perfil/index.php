<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre'] && $_SESSION['rol'] != 5) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlPerfil::singleton_perfil();

$datos = $instancia->mostrarDatosPerfilControl($id_log);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-danger">Perfil</h4>
				</div>
				<div class="card-body">
					<form action="" method="POST" id="form_enviar">
						<input type="hidden" value="<?= $datos['id'] ?>" name="id_log">
						<input type="hidden" value="<?= $datos['password'] ?>" name="pass_old">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="">Documento</label>
									<input type="text" class="form-control numeros" maxlength="50" minlength="1" value="<?= $datos['documento'] ?>" disabled>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="">Nombre</label>
									<input type="text" class="form-control letras" maxlength="50" minlength="1" value="<?= $datos['usuario'] ?>" name="nombre">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="">Correo</label>
									<input type="email" class="form-control" maxlength="50" minlength="1" value="<?= $datos['email'] ?>" disabled>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="">Usuario</label>
									<input type="text" class="form-control" maxlength="50" minlength="1" value="<?= $datos['usuario'] ?>" disabled>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="">Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="password" id="password">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="">Confirmar Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="conf_password" id="conf_password">
								</div>
							</div>
						</div>
						<div class="form-group mt-4 float-right">
							<button type="submit" class="btn btn-danger btn-sm" id="enviar_perfil">
								<i class="fa fa-save"></i>
								&nbsp;
								Guardar Cambios
							</button>
							<input type="hidden" name="perfil" value="<?= $datos['rol'] ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
if (isset($_POST['documento'])) {
	$instancia->editarPerfilControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/validaciones.js"></script>